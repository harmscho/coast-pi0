#include <TPlotter.h>

#include <crs/CorsikaConsts.h>
#include <crs/CParticle.h>
#include <crs/CInteraction.h>
#include <crs/MRunHeader.h>
#include <crs/MEventHeader.h>
#include <crs/MEventEnd.h>

#include <TFile.h>
#include <TH1F.h>
#include <TTree.h>
#include <TROOT.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>

using namespace crs;
using namespace std;

TPlotter::TPlotter()
  {
  cout << "TPlotter::TPlotter()" << endl;
  Clear();
}

TPlotter::~TPlotter() 
{
  Clear();
}

void 
TPlotter::Clear() 
{
  fPrimaryTrack = true;
  fFirstInteraction = false;
  fFirstInteractionX = 0;
  fFirstInteractionY = 0;
  fFirstInteractionZ = 0;
  fFirstInteractionDist = 0;

  fFirst = true;
  
  fEventNo = 0;
  fObservationLevel = 0;
  fHeightFirstInt = 0;
  fZenith = 0;
  fAzimuth = 0;
  fEnergy0 = 0;
  fPrimary = 0;
  
  fCosZenith = 0;
  fSinZenith = 0;
  fCosAzimuth = 0;
  fSinAzimuth = 0;
  
}

void 
TPlotter::SetShowerZenith (float zenith) 
{
  fZenith = zenith*rad;
  fCosZenith = cos (zenith);
  fSinZenith = sin (zenith);
}


void 
TPlotter::SetShowerAzimuth (float azimuth) 
{
  fAzimuth = azimuth*rad;
  fCosAzimuth = cos (azimuth);
  fSinAzimuth = sin (azimuth);
}


void 
TPlotter::SetRunHeader (const crs::MRunHeader &header) 
{
  int run = (int)header.GetRunID ();
  // create filename
  ostringstream fname;
  fname << "DAT" 
	<< setw (6) << setfill('0') << run;
  SetFileName (fname.str ());
}


void 
TPlotter::SetShowerHeader(const crs::MEventHeader &header) 
{
  // to flag the track of the primary particle
  fPrimaryTrack = true;
  if (fStackInput || fPreshower) {
    fPrimaryTrack = false;
    fFirstInteraction = true;
  }
  
  SetEvent (header.GetEventNumber ());
  SetPrimary ((int)header.GetParticleId ());
  SetShowerZenith (header.GetTheta ()*rad);
  SetShowerAzimuth (header.GetPhi ()*rad);    
  SetShowerEnergy (header.GetEnergy ()*GeV);
  SetHeightFirstInt (header.GetZFirst ()*cm);
  SetObservationLevel (header.GetObservationHeight(header.GetNObservationLevels ()-1)*cm);

}
      
void 
TPlotter::SetShowerTrailer (const crs::MEventEnd &trailer) 
{
}

void 
TPlotter::Init() 
{
  cout << "TPlotter::Init() " << endl; 	
  ostringstream file_name;
  file_name << fFileName 	    
	    << "_pi0.root";
  fFile = TFile::Open (file_name.str().c_str(), "RECREATE");
  fFile->cd ();    
  
  fTree = new TTree("t","pi0 stats");
  fEPi0 = new Double_t[100000];
  fXPi0 = new Double_t[100000];
  fYPi0 = new Double_t[100000];
  fZPi0 = new Double_t[100000];
  
  fEPip = new Double_t[100000];
  fXPip = new Double_t[100000];
  fYPip = new Double_t[100000];
  fZPip = new Double_t[100000];
  
  fEPim = new Double_t[100000];
  fXPim = new Double_t[100000];
  fYPim = new Double_t[100000];
  fZPim = new Double_t[100000];
  
  fTree->Branch("ishow",&fIShow);
  fTree->Branch("Pi0_N",&fNPi0);
  fTree->Branch("Pi0_Esum",&fEsumPi0);
  fTree->Branch("Pi0_Emax",&fEmaxPi0);
  fTree->Branch("Pi0_E[Pi0_N]",fEPi0);
  fTree->Branch("Pi0_E0",&fEprim);
  fTree->Branch("Pi0_X[Pi0_N]",fXPi0);
  fTree->Branch("Pi0_Y[Pi0_N]",fYPi0);
  fTree->Branch("Pi0_Z[Pi0_N]",fZPi0);
  
  fTree->Branch("Pip_N",&fNPip);
  fTree->Branch("Pip_Esum",&fEsumPip);
  fTree->Branch("Pip_E[Pip_N]",fEPip);
  fTree->Branch("Pip_X[Pip_N]",fXPip);
  fTree->Branch("Pip_Y[Pip_N]",fYPip);
  fTree->Branch("Pip_Z[Pip_N]",fZPip);
  
  fTree->Branch("Pim_N",&fNPim);
  fTree->Branch("Pim_Esum",&fEsumPim);
  fTree->Branch("Pim_E[Pim_N]",fEPim);
  fTree->Branch("Pim_X[Pim_N]",fXPim);
  fTree->Branch("Pim_Y[Pim_N]",fYPim);
  fTree->Branch("Pim_Z[Pim_N]",fZPim);

}

void
TPlotter::InitEvent()
{  
  //reset counters
  fNPi0 = 0;
  fEsumPi0 = 0;
  fEmaxPi0 = 0;
  fIShow = fEventNo-1;
  fEprim = fEnergy0; 
  
  fNPip = 0;
  fEsumPip = 0;
  
  fNPim = 0;
  fEsumPim = 0;

}

void 
TPlotter::Close () 
{
 fFile->Close();
}

void 
TPlotter::Write () 
{
  
  fTree->Write();
  
    
  Clear();
}
void TPlotter::WriteEvent() {
  fTree->Fill();
}



void 
TPlotter::AddInteraction(const crs::CInteraction &interaction) 
{
  
  if (fPrimaryTrack) 
    fFirstInteraction = true;
  else 
    fFirstInteraction = false;
  
  fPrimaryTrack = false;
}


void 
TPlotter::AddTrack(const crs::CParticle &pre, 
		   const crs::CParticle &post) 
{
  
  /*
    Skip the track of the primary particle, which is in a different 
    reference system as the shower !!!
  */
  if (fPrimaryTrack){
    return;
  }

  if (fFirstInteraction) {    
    fFirstInteraction = false;
    SetFirstInteraction(pre.x, pre.y, pre.z);
  }

  const double e = post.energy/GeV;
  int particleId = (int)pre.particleId;
  
  const bool fconex = (particleId < 0);
  if (fconex) {
    particleId = -particleId;
  }
  
  const double xpi = pre.x;
  const double ypi = pre.y;
  const double zpi = pre.z;

  //pi 0
  if (particleId == 7) {
    // cout << "pi0: " << e << " GeV " << endl; 
    fEsumPi0 += e;

    if (e > fEmaxPi0 ) fEmaxPi0 = e;
    if (fNPi0<1e6) {
      fEPi0[fNPi0] = e;
      fXPi0[fNPi0] = xpi;
      fYPi0[fNPi0] = ypi;
      fZPi0[fNPi0] = zpi;
    }
    fNPi0++;
    
  } else if (particleId == 8){
    fEsumPip += e;
    
    if (fNPip<1e6) {
      fEPip[fNPip] = e;
      fXPip[fNPip] = xpi;
      fYPip[fNPip] = ypi;
      fZPip[fNPip] = zpi;
    }
    fNPip++;
    
  } else if (particleId == 9){
    fEsumPim += e;
    
    if (fNPim<1e6) {
      fEPim[fNPim] = e;
      fXPim[fNPim] = xpi;
      fYPim[fNPim] = ypi;
      fZPim[fNPim] = zpi;
    }
    fNPim++;
    
  } else return;

}


void 
TPlotter::SetFirstInteraction(const double x, const double y, const double z)
{
  fFirstInteractionX = x;
  fFirstInteractionY = y;
  fFirstInteractionZ = z;    
}
