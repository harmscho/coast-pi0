DIR=../out-1000GeV-epos
FILES=$DIR/DAT*
for f in $FILES
do
    if [[ "$f" == *"pi0"* ]]; then
       echo "skipping " $f
     else
	 corsika2root $f $f".root"
    fi	 
done 
    
