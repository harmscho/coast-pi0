#!/bin/bash
CDIR=$PWD

NUMBER_OF_FILES=2

# Define parameters for the simulation
N_SHOWERS=100
EMIN=1000  ##Energy in GeV
EMAX=1000  ##Energy in GeV


OUTDIR=/vol/auger6/harm/corsika/out-"$EMIN"GeV-epos/
CMD=/vol/auger6/harm/corsika/corsika-77410/run/corsika77410Linux_EPOS_urqmd_coast
RUNDIR=$OUTDIR/run_scripts/
mkdir -p $OUTDIR
mkdir -p $RUNDIR
rm $OUTDIR/*


RUN=0
chmod gu+x -R $RUNDIR

while [ $RUN -lt $NUMBER_OF_FILES ]; do
    FILE=$RUNDIR/run$RUN.card        
    #We delete the old files with the same run number, in case they exist
    if [ -f $FILE ]; then
	rm $FILE
    fi

echo   1>&2 "RUNNR   $RUN                       run number
NSHOW   100                            number of showers to generate
PRMPAR  14                            prim. particle (1=gamma, 14=proton, ...)
ESLOPE  -2.                          slope of primary energy spectrum
ERANGE  $EMIN $EMAX                    energy range of primary particle (GeV)
THETAP  0.  0.                        range of zenith angle (degree)
PHIP    -180.  180.                   range of azimuth angle (degree)
SEED    $RANDOM  0 0   seed for 1. random number sequence
SEED    $RANDOM  0 0   seed for 2. random number sequence
SEED    $RANDOM  0 0   seed for 3. random number sequence
OBSLEV  4.5E5                         observation level (in cm) 4.5km
MAGNET  0.01 0.01                     magnetic field centr. Europe
HADFLG  0  0  0  0  0  2              flags hadr.interact.&fragmentation
MUADDI  F                             additional info for muons
MUMULT  T                             muon multiple scattering angle
ELMFLG  F   T                         em. interaction flags (NKG,EGS)
STEPFC  1.0                           mult. scattering step length fact.
RADNKG  200.E2                        outer radius for NKG lat.dens.distr.
EPOPAR input /vol/auger6/harm/corsika/corsika-77410/epos/epos.param        !initialization input file for epos
EPOPAR fname inics /vol/auger6/harm/corsika/corsika-77410/epos/epos.inics  !initialization input file for epos
EPOPAR fname iniev /vol/auger6/harm/corsika/corsika-77410/epos/epos.iniev  !initialization input file for epos
EPOPAR fname initl /vol/auger6/harm/corsika/corsika-77410/epos/epos.initl  !initialization input file for epos
EPOPAR fname inirj /vol/auger6/harm/corsika/corsika-77410/epos/epos.inirj  !initialization input file for epos
EPOPAR fname inihy /vol/auger6/harm/corsika/corsika-77410/epos/epos.ini1b  !initialization input file for epos
EPOPAR fname check none                !dummy output file for epos
EPOPAR fname histo none                !dummy output file for epos
EPOPAR fname data  none                !dummy output file for epos
EPOPAR fname copy  none                !dummy output file for epos
DIRECT $OUTDIR                         output folder
DATDIR  /vol/auger6/harm/corsika/corsika-77410/run/
LONGI   T  10.  F  F                  longit.distr. & step size & fit & outfile
" > $FILE

RUN_FILE=$RUNDIR/Run_${RUN}.sh

if [ -f $RUN_FILE ]; then
        rm $RUN_FILE
fi
echo 1>&2 "#!/bin/bash
$CMD <  $FILE" > $RUN_FILE
chmod 777 $RUN_FILE
#Running using sbatch
mkdir -p $OUTDIR/log/
cd $OUTDIR
sbatch --output=$OUTDIR/log/runlog$RUN -w cn98 --partition=hef $RUN_FILE
echo $RUN
let RUN=RUN+1
done
chmod gu+x -R $OUTDIR
cd $CDIR
