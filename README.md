# Tracking pions in CORSIKA

Set enviroment like this (the funnction SWGO loads ROOT 6.16). 

```
function pi0_env {           
    swgo
    export COAST_DIR=/vol/auger6/harm/corsika/corsika-77410/
    export COAST_USER_LIB=/vol/auger6/harm/corsika/coast-pi0
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${COAST_DIR}/lib/unknown:$COAST_USER_LIB
    export PATH=$PATH:$COAST_DIR/run:/vol/auger6/harm/corsika/corsika2root
}
```
